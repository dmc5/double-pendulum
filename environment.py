import numpy as np
import rungekutta
from collections import deque

class Environment:
    """
    Encapsulates environment for a double pendulum
    """

    def __init__(self,initialState, mass, length, grav,tau):
        self.tau = tau;
        self.mass = mass;
        self.length = length;
        self.grav = grav;

        self.times = deque([0],20000)
        self.states = deque([initialState],20000);

    def getCurrentState(self):
        return self.states[-1];

    def resetEnvironment(self, state):
        self.times.clear();
        self.states.clear();
        self.states.append(state);
        self.times.append(0);
        

    def getNextState(self, action):
        """
        Advances the environment to the next state given an action.
        
        Returns the next state and reward
        """

        nextState, reward = self.simulateAction(self.states[-1], self.times[-1], action);
        self.times.append(self.times[-1]+self.tau)
        self.states.append(nextState);

        if len(self.states) > 20000:
            print("lengths is too long. Something broke");
        
        return nextState, 0

    def simulateAction(self, state, time, action):
        """
        Simulates action given the environment is in state state.

        Does not store result in environment history.
        This is to be used by getNextState and for planning
        """
        nextState = rungekutta.rk4(state, time, self.tau, self.gravrk, action)
        nextState[0] = (nextState[0] + 2*np.pi) % (2*np.pi)
        nextState[1] = (nextState[1] + 2*np.pi) % (2*np.pi)
        return nextState, 0

    def generatePositions(self):
        """
        Converts the thetas to x and y plots
        """
        temp = np.array(list(self.states));
        theta1 = temp[:,0];
        theta2 = temp[:,1];
        theta1dot = temp[:,2];
        theta2dot = temp[:,3];
        
        xplot1 = self.length[0] * np.sin(theta1);
        yplot1 = -1* self.length[0] * np.cos(theta1);
        xplot2 = xplot1 + self.length[1] * np.sin(theta2);
        yplot2 = yplot1 - self.length[1] * np.cos(theta2);

        return xplot1, yplot1, xplot2, yplot2;

    def getAngleAndTime(self):
        """
        Returns the times, and both thetas for plotting
        """
        temp = np.array(list(self.states));
        return self.times, temp[:,0], temp[:,1];

    def gravrk(self,s,t,action):
        """
        Using by rk4 to simulate the double pendulum
        """
        deriv = np.zeros(4);
        theta1 = s[0];
        theta2 = s[1];
        theta1dot = s[2];
        theta2dot = s[3];

        A1 = self.mass[1]*self.length[0]*self.length[1]*theta2dot*np.sin(theta1-theta2)*(theta1dot-theta2dot) + \
            -self.mass[1]*self.length[0]*self.length[1]*theta1dot*theta2dot*np.sin(theta1-theta2) + \
            -(self.mass[0]+self.mass[1])*self.grav*self.length[0]*np.sin(theta1);
        B1 = (self.mass[0]+self.mass[1]) * self.length[0]**2;
        C1 = self.mass[1]*self.length[0]*self.length[1]*np.cos(theta1-theta2);

        A2 = self.mass[1]*self.length[0]*self.length[1]*theta1dot*np.sin(theta1-theta2)*(theta1dot-theta2dot) + \
             +self.mass[1]*self.length[0]*self.length[1]*theta1dot*theta2dot*np.sin(theta1-theta2) + \
             -self.mass[1]*self.grav*self.length[1]*np.sin(theta2);
        B2 = self.mass[1]*self.length[0]*self.length[1]*np.cos(theta1-theta2)
        C2 = self.mass[1]*self.length[1]**2

        deter = B1*C2 - C1*B2

        deriv[0] = theta1dot;
        deriv[1] = theta2dot;
        deriv[2] = 1/deter * (C2*A1 - C1*A2);
        deriv[3] = 1/deter * (-B2*A1 + B1*A2);

        # Making the motor apply a fixed angular acceleration for now
        moment_of_inertia = 1;
        
        deriv[2] = deriv[2] + action / moment_of_inertia;
        
        return deriv;
