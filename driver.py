import numpy as np
import environment
import random
import json
import matplotlib.pyplot as plt

barriers   = [3*np.pi/4.0, 44/45.0*np.pi, 46/45.0*np.pi, 5*np.pi/4.0];
barriers_2 = barriers;
barriersDot   = [-np.pi/45.0, np.pi/45.0 ,float('inf')];
barriersDot_2 = barriersDot;
STATES = (len(barriers)-1)*(len(barriers_2)-1)*len(barriersDot)*len(barriersDot_2);
TORQUE = 1000;
L = [1,1]
m = [10,10]
grav = 9.81
tau = 0.001;

def classify(state):
    """
    Takes in a state and returns a number    
    """
    num = 0;
    if state[0] < barriers[0] or state[0] > barriers[-1] or state[1] < barriers[0] or state[1] > barriers[-1]:
        return -1;

    #Tile theta1
    for i in range(len(barriers)-1):
        if state[0] < barriers[i+1]:
            num = i
            break;

    #Tile for theta2
    for i in range(len(barriers_2)-1):
        if state[1] < barriers_2[i+1]:
            num += i*(len(barriers)-1);
            break;
    
    #Tile for theta1 dot
    for i in range(len(barriersDot)):
        if state[2] < barriersDot[i]:
            num += (i)*(len(barriers_2)-1)*(len(barriers)-1);
            break;

    #Tile for theta2dot
    for i in range(len(barriersDot_2)):
        if state[3] < barriersDot_2[i]:
            num += (i) * (len(barriersDot)) * (len(barriers_2)-1)*(len(barriers)-1);
            break;
    return num

def getNextAction(state, weights):
    """
    Randomly returns the next action given the state and the weights

    Needs to first computer the numerical preference for each action h(s,a,weights).

        h(s,a,weights) is just the dot product of weights and feature vector

        The feature vector I am using is just 1 if the state is in that region and 0 otherwise

        So first need to catagoize the state then += 1024*i for the ith action

        The numerical preference for this state-action pair is the weight at this index

    Then computers the probabilites each will occur

    Then randomly selects and returns an action
    
    """
    actions = [0,1,2];

    actionProbabilities = policyProbability(state,weights)

    randNum = random.random();

    if randNum < actionProbabilities[0]:
        return actions[0];
    elif randNum < (actionProbabilities[0] + actionProbabilities[1]):
        return actions[1];
    else:
        return actions[2];
    
def policyProbability(state,weights):
    actions = [0,1,2];
    actionPreference = np.zeros(3);

    stateNum = classify(state);
    for i in range(len(actions)):
        stateActionNum = stateNum + i*STATES;
        actionPreference[i] = weights[stateActionNum];

    actionProbabilities = []
    aggr = 0;

    for i in range(len(actions)):
        actionProbabilities.append(np.exp(np.minimum(4.0, np.maximum(-4.0, actionPreference[i]))));
        aggr += actionProbabilities[i];

    
    actionProbabilities = [i/aggr for i in actionProbabilities];

    if np.abs(np.sum(actionProbabilities) - 1) > 1e-8:
        print("Action probabilities greater than 1");
        
    return actionProbabilities
    

def getStateValue(state, stateWeights):
    stateNum = classify(state);
    if stateNum == -1:
        return 0;
    else:
        return stateWeights[stateNum];

def getStateValueGrad(state,length):
    stateNum = classify(state);
    grad = np.zeros(length);
    grad[stateNum] = 1;
    return grad;


def getPolicyGrad(action,state,weights):
    actions = [0,1,2];
    grad = np.zeros(len(weights));
    stateNum = classify(state)
    actionProbabilities = policyProbability(state,weights);
    
    for i in actions:
        grad[stateNum + STATES*i] = -actionProbabilities[i];
        if i == action:
            grad[stateNum + STATES*i] += 1

    return grad

def actorCritic(env):
    iters = 0;
    
    lambdaTh = 0.9;
    lambdaSt = 0.8;
    a = 1000;
    b = 0.5;
    gamma = 0.95;

    weights = np.zeros(STATES*3);
    stateWeights = np.zeros(STATES);
    
    
    while True:
        initialTheta1 = np.random.normal(np.pi, np.pi/60.0, 1)[0];
        initialTheta2 = np.random.normal(np.pi, np.pi/60.0, 1)[0];
        initialAngular1 = np.random.normal(0, np.pi/60.0,1)[0];
        initialAngular2 = np.random.normal(0, np.pi/60.0,1)[0];
        env.resetEnvironment(np.array([initialTheta1, initialTheta2,initialAngular1,initialAngular2]));
        
        state = env.getCurrentState();
        eTheta = np.zeros(STATES*3);
        eState = np.zeros(STATES);
        I = 1;
        count = 0;
        
        while classify(state) != -1:
            
            action = getNextAction(state,weights);
        
            if action == 0:
                torque = -1*TORQUE;
            elif action == 1:
                torque = 0;
            else:
                torque = TORQUE;

            for i in range(1):
                nextState, reward = env.getNextState(torque);

            reward = 0;
            
            if classify(nextState) == -1:
                reward = -1;

            delta = reward + gamma*getStateValue(nextState,stateWeights) - getStateValue(state, stateWeights);

            eState = lambdaSt * eState + I * getStateValueGrad(state, STATES);

            eTheta = lambdaTh * eTheta + I * getPolicyGrad(action, state, weights);

            stateWeights = stateWeights + b*delta*eState;

            weights = weights + a*delta*eTheta;

            I = gamma*I;

            state = nextState;

            count += 1;

            if count > 20000:
                break

        iters += 1;

        if iters > 200:
            return weights, stateWeights;
                   
def simulate(env):
    for i in range(20000):
        env.getNextState(0);

def simulateWithAction(env, weights):
    for i in range(20000):
        state = env.getCurrentState();
        if classify(state) == -1:
            print("Breaking Early at " + str(i) + " iteration");
            break;
        action = getNextAction(state,weights);
        
        if action == 0:
            torque = -1*TORQUE;
        elif action == 1:
            torque = 0;
        else:
            torque = TORQUE;
            
        env.getNextState(torque);
        
env = environment.Environment(np.zeros(4), m, L, grav, tau)

print("Starting Training");
while True:
	weights, stateWeights = actorCritic(env);
	env.resetEnvironment(np.array([np.pi + np.pi/120.0, np.pi,0,0]));
	simulateWithAction(env,weights);
	xplot1, yplot1, xplot2, yplot2 = env.generatePositions();
	if len(xplot1) > 10000:
		break;

print("Saving Sim");

with open("positions",'w') as f:
    json.dump(list(xplot1)+list(yplot1)+list(xplot2)+list(yplot2),f);

with open("weights",'w') as f:
    json.dump(list(weights),f);
	
with open("stateWeights",'w') as f:
    json.dump(list(stateWeights),f);

print("Making Plots")

timePlot, anglePlot, angleTheta2 = env.getAngleAndTime();
plt.figure();
plt.plot(timePlot,anglePlot,label="Fixed Pendulum");
plt.plot(timePlot,angleTheta2,label="Free Pendulum");
plt.plot(timePlot, np.pi*np.ones(len(timePlot)));
axes = plt.gca()
axes.set_ylim([3/4.0*np.pi,5/4.0*np.pi])
plt.legend();
plt.show();
